# CI/CD components for Docker

This project provides [CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
to build, test, and deploy container images using [Docker](https://www.docker.com).
